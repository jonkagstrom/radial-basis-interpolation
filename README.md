# Java Compact Support Radial Basis Interpolation #

An java example implementation of Compact Support [Radial Basis](https://en.wikipedia.org/wiki/Radial_basis_function)
interpolation over a regular grid. 

It uses the [Wendland C2 functions](http://eprints.maths.ox.ac.uk/1513/1/NA-12-10.pdf) and can be run 
for any degree (although it will get slow pretty quickly).

The distance matrix is pre calculated and inverted, so solving the linear system can be done
by multiplying the values vector during run time.

The epsilon parameter describes how far a point affects the interpolation. By setting it to the square root of the degree it seems like the grid boundaries always are fully continuous. With larger values the boundaries become visible, not sure why (as long we are within the degree range) maybe due to precision loss?

There is a dependency on [JAMA](http://math.nist.gov/javanumerics/jama/) for the matrix inversion.

Disclaimer: I am not 100% sure I've understood everything correctly, so the code may be buggy.

## How does it look? ##
White noise interpolated with d=4, eps=3, f=30:
![csrbf](http://www.playchilla.com/wp-content/uploads/2016/11/csrbf_d4_e3.png)

White noise interpolated with d=2, e=2, f=30:
![csrbf](http://www.playchilla.com/wp-content/uploads/2016/11/csrbf_d2_e2.png)

White noise interpolated with d=3, e=sqrt(3), f=150:
![csrbf](http://www.playchilla.com/wp-content/uploads/2016/11/csrbf_d3_esqrt3.png)