package rbf;

/**
 * Degree 2 CSRBF that runs faster than the general. It's possible
 * to do this for degree 3,4 as well. I think with degree 3 you get
 * 10 coefficients and 21 with degree 4.
 * <p/>
 * Repo: https://bitbucket.org/jonkagstrom/radial-basis-interpolation
 * by @jonkagstrom, 2016, playchilla.com
 */
public class Degree2RbfInterpolation {
    private static final long BITS = 53;
    private static final long BIT_MASK = (1L << BITS) - 1L;
    private static final double NORMALIZER = 1. / (1L << (BITS - 1));

    // Can't change this radius scale since the matrix has been pre calculated for sqrt(2)
    private static final double _scale = 1. / Math.sqrt(2);
    // Pre calculated coefficients for inverse of distance matrix for degree 2 and eps=Math.sqrt(2)
    private static final double a = 1.0015926724899984;
    private static final double b = -0.028264338850564497;
    private static final double c = 0.001592672489998265;

    public Degree2RbfInterpolation() {
    }

    public double getValue(final double x, final double y) {
        final long cx0 = _floorToLong(x);
        final long cy0 = _floorToLong(y);
        final double u = x - cx0;
        final double v = y - cy0;

        final double v0 = _n(cx0, cy0);
        final double v1 = _n(cx0 + 1, cy0);
        final double v2 = _n(cx0, cy0 + 1);
        final double v3 = _n(cx0 + 1, cy0 + 1);

        // Solve Ax=b by multiplying b with A inverse
        final double bv0 = b * v0;
        final double bv1 = b * v1;
        final double bv2 = b * v2;
        final double bv3 = b * v3;
        final double w0 = a * v0 + bv1 + bv2 + c * v3;
        final double w1 = bv0 + a * v1 + c * v2 + bv3;
        final double w2 = bv0 + c * v1 + a * v2 + bv3;
        final double w3 = c * v0 + bv1 + bv2 + a * v3;

        double sum = 0;
        sum += w0 * _dist(0, 0, u, v);
        sum += w1 * _dist(1, 0, u, v);
        sum += w2 * _dist(0, 1, u, v);
        sum += w3 * _dist(1, 1, u, v);
        return sum;
    }

    private double _dist(double px, double py, double u, double v) {
        final double dx = u - px;
        final double dy = v - py;
        final double r = _scale * Math.sqrt(dx * dx + dy * dy);
        // Wendland C2 compactly support function for d<=3
        final double ir = Math.max(0, 1. - r);
        return ir * ir * ir * ir * (4. * r + 1.);
    }

    private static double _n(final long x, final long y) {
        long c = (x * 73856093L) ^ (y * 83492791L);
        c ^= (c >>> 23);
        c *= 0x2127599bf4325c37L;
        c ^= (c >>> 47);
        return NORMALIZER * (c & BIT_MASK) - 1.;
    }

    private static long _floorToLong(final double x) {
        final long xi = (long) x;
        return x < xi ? xi - 1 : xi;
    }
}
