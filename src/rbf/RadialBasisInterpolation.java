package rbf;

import Jama.Matrix;

/**
 * Compact Support Radial Basis Interpolation (CSRBF)
 * <p/>
 * CSRBF implementation for arbitrary degree that interpolates
 * over white noise. All points are on a regular grid so the
 * inverse distance matrix is pre calculated.
 * <p/>
 * I was too lazy to implement matrix inversion so there is a
 * dependency on JAMA (http://math.nist.gov/javanumerics/jama/)
 * <p/>
 * Repo: https://bitbucket.org/jonkagstrom/radial-basis-interpolation
 * by @jonkagstrom, 2016, playchilla.com
 */
public class RadialBasisInterpolation {
    private static final long BITS = 53;
    private static final long BIT_MASK = (1L << BITS) - 1L;
    private static final double NORMALIZER = 1. / (1L << (BITS - 1));

    private final int _degree;
    private final double _scale;
    private final double[][] _inverseDistance;
    private final double[] _values;
    private final double[] _tmp;
    private final int _wendlandPow;

    public RadialBasisInterpolation(int degree) {
        this(degree, Math.sqrt(degree));
    }

    public RadialBasisInterpolation(int degree, double epsilon) {
        _degree = degree;
        _wendlandPow = 3 + degree / 2;
        _scale = 1. / epsilon;
        _inverseDistance = _getInverseDistanceMatrix();
        _values = new double[_degree * _degree];
        _tmp = new double[_degree * _degree];
    }

    public double getValue(double x, double y) {
        long cx0 = _floorToLong(x);
        long cy0 = _floorToLong(y);
        double u = x - cx0;
        double v = y - cy0;

        for (int i = 0; i < _degree; ++i) {
            for (int j = 0; j < _degree; ++j) {
                _values[j + i * _degree] = _n(cx0 + j, cy0 + i);
            }
        }

        double[] weights = _multiply(_inverseDistance, _values);
        double sum = 0;
        for (int py = 0; py < _degree; ++py) {
            for (int px = 0; px < _degree; ++px) {
                double d = _dist(px, py, u, v);
                sum += d * weights[px + _degree * py];
            }
        }
        return _clamp(sum, -1, 1);
    }

    private double _dist(double px, double py, double u, double v) {
        double dx = u - px;
        double dy = v - py;
        double r = _scale * Math.sqrt(dx * dx + dy * dy);
        // Wendland C2 compactly support rbf functions for d,
        // found here: http://eprints.maths.ox.ac.uk/1513/1/NA-12-10.pdf
        double ir = Math.max(0, 1. - r);
        double irPow = ir;
        for (int i = 0; i < _wendlandPow; ++i) {
            irPow *= ir;
        }
        return irPow * (1 + _wendlandPow * r);
    }

    private double[] _multiply(double[][] m, double[] values) {
        int n = values.length;
        for (int i = 0; i < n; ++i) {
            _tmp[i] = 0;
            for (int j = 0; j < n; ++j) {
                _tmp[i] += values[j] * m[j][i];
            }
        }
        return _tmp;
    }

    private double[][] _getInverseDistanceMatrix() {
        int n = _degree * _degree;
        double[][] m = new double[n][n];
        for (int y = 0; y < n; ++y) {
            for (int x = 0; x < n; ++x) {
                int ax = y % _degree;
                int ay = y / _degree;
                int bx = x % _degree;
                int by = x / _degree;
                m[x][y] = _dist(ax, ay, bx, by);
            }
        }

        // Using JAMA to invert matrix
        return new Matrix(m).inverse().getArray();
    }

    // positional noise as described here: https://bitbucket.org/jonkagstrom/b-spline-noise
    private static double _n(long x, long y) {
        long c = (x * 73856093L) ^ (y * 83492791L);
        c ^= (c >>> 23);
        c *= 0x2127599bf4325c37L;
        c ^= (c >>> 47);
        return NORMALIZER * (c & BIT_MASK) - 1.;
    }

    private static long _floorToLong(double x) {
        long xi = (long) x;
        return x < xi ? xi - 1 : xi;
    }

    private static double _clamp(double v, double min, double max) {
        if (v < min) {
            return min;
        }
        if (v > max) {
            return max;
        }
        return v;
    }
}
